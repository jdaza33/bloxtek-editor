/**
 *
 */

const express = require('express')
const cors = require('cors')
const path = require('path')
const PORT = process.env.PORT || 3100

const app = express()

app.use(cors())
app.use(express.json())

app.get('/', function (req, res) {
  res.sendFile(__dirname + '/index.html')
})

app.listen(PORT, () => console.log(`Running in ${PORT}...`))
